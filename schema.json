{
  "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
  "name": "menus-collectifs",
  "title": "Menus de la restauration collective",
  "description": "Schéma permettant de décrire les ingrédients entrant dans la composition des plats des menus proposés par des collectivités locales et des établissements publics. Il permet de préciser les nutriments et allergènes présent et d'ajouter une vision diététique aux aspects économiques et agro-environnementaux présents dans les schémas (menus et plats) liés.",
  "countryCode": "FR",
  "homepage": "https://gitlab.com/opendatafrance/scdl/ingredients-plats",
  "path": "https://gitlab.com/opendatafrance/scdl/ingredients-plats/raw/v0.2/schema.json",
  "resources": [
    {
      "title": "Exemple de fichier de menus valide en csv",
      "path": "https://gitlab.com/opendatafrance/scdl/ingredients-plats/raw/v0.2/examples/ingredients-plats_valide.csv"
    },
    {
      "title": "Exemple de fichier de menus valide en Office Open XML",
      "path": "https://gitlab.com/opendatafrance/scdl/ingredients-plats/raw/v0.2/examples/ingredients-plats_valide.xlsx"
    }
  ],
  "sources": [
    {
      "title": "Décret n° 2008-1153 du 7 novembre 2008 concernant la liste des ingrédients allergènes majeurs devant figurer sur l'étiquetage des denrées alimentaires.",
      "path": "https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000019735750"
    },
    {
      "title": "Règlement (UE) n°1169/2011 concernant l’information du consommateur sur les denrées alimentaires et notamment ses articles 9, 21, 44 et l’annexe II sur les substances ou produits provoquant des allergies ou intolérances.",
      "path": "http://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32011R1169&from=fr"
    },
    {
      "title": "Décret n°2015-447 du 17 avril 2015 relatif à l’information des consommateurs sur les allergènes et les denrées alimentaires non préemballées.",
      "path": "http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030491684&categorieLien=id"
    },
    {
      "title": "Loi n° 2018-938 du 30 octobre 2018 pour l'équilibre des relations commerciales dans le secteur agricole et alimentaire et une alimentation saine, durable et accessible à tous.",
      "path": "https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037547946"
    },
    {
      "title": "Décret n° 2011-1227 du 30 septembre 2011 relatif à la qualité nutritionnelle des repas servis dans le cadre de la restauration scolaire",
      "path": "https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024614716"
    }
  ],
  "created": "2020-11-19",
  "lastModified": "2023-11-08",
  "version": "0.2",
  "contributors": [
    {
      "title": "OpenDataFrance",
      "role": "author"
    },
    {
      "title": "Pascal Romain",
      "organisation": "Rhizome data",
      "email": "pascal@rhizome-data.fr",
      "role": "author"
    },
    {
      "title": "Thierry Martin",
      "organisation": "Conseil Départemental de Haute-Garonne",
      "email": "thierry.martin@cd31.fr",
      "role": "contributor"
    },
    {
      "title": "Charles Nepote",
      "organisation": "OpenFoodFacts",
      "email": "charles@openfoodfacts.org",
      "role": "contributor"
    },
    {
      "title": "René Marrot",
      "organisation": "Agglomération du Grand Poitiers",
      "email": "rene.marrot@grandpoitiers.fr",
      "role": "contributor"
    },
    {
      "title": "Antoine Augusti",
      "organisation": "Etalab",
      "email": "antoine.augusti@data.gouv.fr",
      "role": "contributor"
    },
    {
      "title": "Amélie Rondot",
      "organisation": "multi",
      "email": "amelie.rondot@multi.coop",
      "role": "contributor"
    }
  ],
  "fields": [
    {
      "name": "produitCode",
      "title": "Code d'identification du produit",
      "description": "Cet identifiant permet de faire la jointure avec le schéma des plats composants les menus de la restauration collective.",
      "type": "string",
      "examples": "58765876",
      "constraints": {
        "required": true
      }
    },
    {
    "name": "ingredientCode",
    "title": "Identifiant de l'ingrédient",
    "description": "l'ingrédient est un élément théorique utilisé dans la définition d'une fiche technique (= une \"recette\" pour le grand public), stable dans le temps. L'ingrédient ne présuppose rien des conditions de production (qualité et labels) ni des conditions d'approvisionnement (conditionnement, fournisseur, marché, transport, prix …), etc..En revanche il peut porter les caractéristiques nutritionnelles théoriques (calories, répartition en lipides glucides, protides, etc.).",
    "type": "string",
    "examples": "c12987987",
    "constraints": {
      "required": true
    }
  },
  {
    "name": "ingredientNom",
    "title": "Nom de l'ingrédient",
    "description": "nom donné à l'ingrédient entrant dans la composition du plat.",
    "type": "string",
    "examples": "huile d'olive",
    "constraints": {
      "required": true
    }
  },
  {
    "name": "ingredientCategorie",
    "title": "Catégorie de l'ingrédient",
    "description": "Catégorisation de l'ingrédient entrant dans la composition du plat.",
    "type": "string",
    "examples": "huile",
    "constraints": {
      "required": false
    }
  },
  {
    "name": "ingredientComposition",
    "title": "Quantité de l'ingrédient utilisée pour la composition du plat",
    "description": "En fonction des recettes les ingrédients entrant dans la composition des plats interviennent en quantité variable.",
    "type": "string",
    "examples": "25g",
    "constraints": {
      "required": false
    }
  },
  {
    "name": "ingredientAllergene",
    "title": "Nom des allergènes présents dans l'ingrédient",
    "description": "Enumération des éventuels allergènes (séparés par des virgules) présents dans les ingrédients entrant dans la composition des plats proposés. Actuellement la distinction n'est pas faite entre les allergènes présents du fait de la recette (fiche technique) ou sous forme de traces (lieu de production).",
    "type": "string",
    "examples": "Fruits à coques",
    "constraints": {
      "required": false,
      "enum": [
        "Céréales contenant du gluten",
        "Crustacés",
        "Oeufs",
        "Poissons",
        "Arachides",
        "Soja",
        "Lait",
        "Fruits à coques",
        "Céleri",
        "Moutarde",
        "Graines de sésame",
        "Anhydride sulfureux et sulfites",
        "Lupin",
        "Mollusques"
      ]
    }
  },        
  {
    "name": "ingredientNutrimentNom",
    "title": "Nom des nutriments présents dans l'ingrédient",
    "description": "Les ingrédients composant les produits contiennent différents nutriments. Cette information est notamment utilisée pour composer des menus équilibrés du point de vue diététique.",
    "type": "string",
    "examples": "glucide",
    "constraints": {
      "required": false
    }
  },
  {
    "name": "ingredientNutrimentcomposition",
    "title": "Quantité de nutriment présents dans l'ingrédient",
    "description": "Les ingrédients composant les produits contiennent différents nutriments en quantité variable. Ce champ permet d'indiquer la quantité de nutriment par gramme.",
    "type": "number",
    "examples": "14",
    "constraints": {
      "required": false
    }
  }
],
"missingValues":[
  ""
]
}

