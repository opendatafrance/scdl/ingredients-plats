# Changelog

## 0.2
* Migration de l'instance gitlab d'hébergement du schéma Ingredients vers l'instance publique https://gitlab.com/opendatafrance/scdl/ingredients-plats


## 0.1

* Création à partir des exemples de fichiers publiés et de discussion avec des représentant des collectivités locales.